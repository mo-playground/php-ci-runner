cat << EOF > config.json
{
    "auths": {
        "$CI_REGISTRY": {
            "username": "$CI_REGISTRY_USER",
            "password": "$CI_REGISTRY_PASSWORD"
        }
    }
}
EOF
